const innerMain = document.querySelector('.inner-main');
const innerMainH3 = document.getElementById('iM-h1');
const bg = document.querySelector('.bg');

function lookAround() {
    innerMainH3.classList.toggle('h3-show');
    innerMain.classList.toggle('inner-main-active');
    bg.classList.toggle('bg-active');
}